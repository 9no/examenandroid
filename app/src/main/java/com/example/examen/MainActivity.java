package com.example.examen;

import android.os.Bundle;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    private EditText txtUsuario;
    private Button btnEntrar, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtUsuario = findViewById(R.id.txtUsuario);
        btnEntrar = findViewById(R.id.btnIngresar);
        btnSalir = findViewById(R.id.btnSalir);




        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtUsuario.getText().toString();

                if (!nombre.isEmpty()){
                    Toast.makeText(MainActivity.this, "Mi nombre es, " + nombre + "!", Toast.LENGTH_SHORT).show();



                    // Se manda a llamar la Actividad del Cálculo del rectángulo
                    Intent intent = new Intent(MainActivity.this, RectanguloActividad.class);
                    intent.putExtra("nombre", txtUsuario.getText().toString());
                    startActivity(intent);
                }else{
                    Toast.makeText(MainActivity.this, "Ingresa tu nombre.", Toast.LENGTH_SHORT).show();
                }


            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}