package com.example.examen;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class RectanguloActividad extends AppCompatActivity {

    private EditText editBase, editAltura;
    private TextView txtPerimetro, txtArea, textViewNombre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo_actividad);

        editBase = findViewById(R.id.txtBase);
        editAltura = findViewById(R.id.txtAltura);
        txtPerimetro = findViewById(R.id.txtPerimetro);
        txtArea = findViewById(R.id.txtArea);
        textViewNombre = findViewById(R.id.textViewNombre);

        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        textViewNombre.setText("Mi nombre es: "+nombre);

        Button btnCalcular = findViewById(R.id.btnCalcular);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        Button btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        Button btnRegresar = findViewById(R.id.btnRegresar);
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    public void calcular() {
        String baseText = editBase.getText().toString().trim();
        String alturaText = editAltura.getText().toString().trim();

        if (!baseText.isEmpty() && !alturaText.isEmpty()) {
            int base = Integer.parseInt(baseText);
            int altura = Integer.parseInt(alturaText);

            Rectangulo rectangulo = new Rectangulo(base, altura);

            int area = Math.round(rectangulo.calcularArea());
            int perimetro = Math.round(rectangulo.calcularPerimetro());

            txtArea.setText(String.valueOf(area));
            txtPerimetro.setText(String.valueOf(perimetro));
        } else {
            Toast.makeText(this, "Por favor, ingresa los valores de base y altura.", Toast.LENGTH_SHORT).show();
        }
    }

    public void limpiar() {
        editBase.setText("");
        editAltura.setText("");
        txtArea.setText("");
        txtPerimetro.setText("");
    }

    public void salir() {
        finish();
    }
}